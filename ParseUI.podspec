
Pod::Spec.new do |s|

  s.name         = "ParseUI"
  s.version      = "1.1.4"
  s.summary      = "ParseUI framework."

  s.description  = <<-DESC
                   ParseUI framework.

                   DESC

  s.homepage     = "https://parse.com/apps/quickstart#parse_data/mobile/ios/native/new"

s.platform     = :ios, "8.0"

  s.source       = { :git => "git@bitbucket.org:project0-1/parseui.git" }


  s.source_files  = "*.*", "Headers/*.{h,m,swift}", "Modules/*.modulemap"



end
